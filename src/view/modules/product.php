<div id="product">

<div>

  <div class="product-images">
    <img id="splash" src="/public/images/<?= $params["product"]["image"] ?>" />
    <div class="product-miniatures">
      <div>
        <img src="/public/images/<?= $params["product"]["image"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt1"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt2"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt3"] ?>" />
      </div>
    </div>
  </div>

  <div class="product-infos">
    <p class="product-category">
      <?= $params["product"]["category"] ?>
    </p>
    <h1><?= $params["product"]["name"] ?></h1>
    <p class="product-price">
      <?= $params["product"]["price"] ?>€
    </p>
    <form>
      <button id="btn_minus" type="button">-</button>
      <button id="btn_quantity" type="button">1</button>
      <button id="btn_plus" type="button">+</button>
      <input type="submit" value="Ajouter au panier" />
    </form>
    <p id="box_quantity" class="box error">
      Quantité maximale autorisée !
    </p>
  </div>

</div>

<div>
  <div class="product-spec">
    <h2>Spécificités</h2>
    <?= $params["product"]["spec"] ?>  
  </div>
  <div class="product-comments">
    <!-- TODO: Gérer les commentaires -->
    <h2>Avis</h2>
    <p>
      Il n'y a pas d'avis sur ce produit.
    </p>
    <form action="/" method="post">
      <input type="text" placeholder="Rédiger un commentaire" />
    </form>
  </div>
</div>

</div>

<script src="/public/scripts/product.js"></script>