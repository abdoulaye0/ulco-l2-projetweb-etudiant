<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $list = \model\StoreModel::listProducts();

    // Variables transmises à la vue
    $params = array(
      "module" => "store.php",
      "title" => "Store",
      "categories" => $categories,
      "list" => $list
    );

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function product(int $id): void
  {
    // Communications avec la base de données
    $product = \model\StoreModel::infoProduct($id);

    // Si le produit n'existe pas
    if ($product == null) {
      header("Location: /store");
      exit();
    }

    // Variables transmises à la vue
    $params = [
      "module"  => "product.php",
      "title"   => "Produit " . $id,
      "product" => $product
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

}