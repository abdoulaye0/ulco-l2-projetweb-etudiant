<?php

namespace controller;

class ErrorController {

  public function error(): void
  {
    // Variables à transmettre à la vue
    $params = [
      "module" => "error.php",
      "title"  => "Erreur",
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

}