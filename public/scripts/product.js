// Cacher la box "quantité dépassée"
box_quantity.style.display = 'none';

// Evénement clic sur les images miniatures
document.querySelectorAll('.product-miniatures img').forEach(img => {
  img.onclick = () => {
    splash.src = img.src;
  }
});

btn_minus.onclick = () => {
  const v = parseInt(btn_quantity.innerText);
  const vnew = v <= 1 ? 1 : v - 1;
  box_quantity.style.display = 'none';
  btn_quantity.innerText = vnew;
}

btn_plus.onclick = () => {
  const v = parseInt(btn_quantity.innerText);
  const vnew = v >= 5 ? 5 : v + 1;
  if (vnew >= 5) {
    box_quantity.style.display = 'inline-block';
  }
  btn_quantity.innerText = vnew;
}