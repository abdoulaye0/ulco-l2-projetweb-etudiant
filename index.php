<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

// GET "/store"
$router->get('/store', 'controller\StoreController@store');

// GET "/store/{:num}"
$router->get('/store/{:num}', 'controller\StoreController@product');

//GET "/account"
$router->get('/account', 'controller\AccountController@account');

// get "post"
$router->post("/une/route/post", "Controller@callback");

//GET "/login"
$router->get('/login','controller\AccountController@login') ;

// GET "/signin"
$router->get('/signin','controller\AccountController@signin');


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');



/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
